@sprint-3
Feature: Support internationalisation
    "**In order to** practice my use of greetings in several languages"
    "**As a** polyglot coffee lover"
    "**I can** select the language on the coffee machine"

  @priority-medium
  Scenario Outline: Messages are based on language (<hiptest-uid>)
    When I start the coffee machine using language "<language>"
    Then message "<ready_message>" should be displayed

    Examples:
      | ready_message | language | hiptest-uid |
      | Ready | en | uid:cc575e18-5096-4977-8a72-402381ec5382 |
      | Pret | fr | uid:9eeddc64-f3de-4f14-9d0f-3bcfa067038c |

  @priority-medium
  Scenario: No messages are displayed when machine is shut down (uid:daeee6fd-f9b6-48e0-8789-2e64a5a62fef)
    Given the coffee machine is started
    When I shutdown the coffee machine
    Then message "" should be displayed
