@sprint-2
Feature: Can be configured
    "**In order to** get the best possible coffees"
    "**As a** geeky coffee lover"
    "**I can** configure it to match my needs"

  @priority-medium
  Scenario: Display settings (uid:7bd138a0-f26d-467d-b7d9-0e3034889ae8)
    Given the coffee machine is started
    When I switch to settings mode
    Then displayed message is:
      """
      Settings:
       - 1: water hardness
       - 2: grinder
      """

  @priority-high
  Scenario: Default settings (uid:e818e442-aa4e-4265-9de1-aae69d2694fe)
    Given the coffee machine is started
    When I switch to settings mode
    Then settings should be:
      | water hardness | 2      |
      | grinder        | medium |
