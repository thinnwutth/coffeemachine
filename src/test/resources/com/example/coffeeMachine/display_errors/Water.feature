Feature: Water
    As a coffee lover
    I have to handle the water tank
    So I can have coffee when I need it

  Background:
    Given the coffee machine is started
    And I handle everything except the water tank

  @priority-high
  Scenario: When the water tank is filled, the message disappears (uid:abfeb316-a4e5-46d0-8422-5d63153f4ee8)
    When I take "55" coffees
    And I fill the water tank
    Then message "Ready" should be displayed

  @priority-low
  Scenario: It is possible to take 10 coffees after the message "Fill water tank" is displayed (uid:b74d510d-fea0-4721-8aa5-921d3f38780f)
    When I take "60" coffees
    Then coffee should be served
    When I take a coffee
    Then coffee should not be served

  @priority-high
  Scenario: Message "Fill water tank" is displayed after 50 coffees are taken (uid:06f491cc-3540-4b5d-befe-0cbc9db3ae1a)
    When I take "50" coffees
    Then message "Fill tank" should be displayed
