Feature: Beans
    As a coffee lover
    I have to put fresh beans from time to time
    So I can have coffee when I need it

  Background:
    Given the coffee machine is started
    And I handle everything except the beans

  @priority-high
  Scenario: After adding beans, the message "Fill beans" disappears (uid:0db76b51-5938-4aa5-9402-1d2c9f96cee1)
    When I take "40" coffees
    And I fill the beans tank
    Then message "Ready" should be displayed

  @priority-low
  Scenario: It is possible to take 40 coffees before there is really no more beans (uid:f40f4caa-afc2-4667-accf-2b8285859628)
    When I take "40" coffees
    Then coffee should be served
    When I take a coffee
    Then coffee should not be served
    And message "Fill beans" should be displayed

  @priority-high
  Scenario: Message "Fill beans" is displayed after 38 coffees are taken (uid:ad65f66e-8e29-4945-bed2-93a841c5eca4)
    When I take "38" coffees
    Then message "Fill beans" should be displayed
